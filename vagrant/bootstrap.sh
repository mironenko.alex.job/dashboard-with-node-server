#!/usr/bin/env bash
# change dns nameserver
echo "------------"
echo " nameserver "
echo "------------"
echo "nameserver 8.8.8.8" > /etc/resolv.conf

# prepare box
echo "-------------"
echo " prepare box "
echo "-------------"
apt-get update
apt-get upgrade
apt-get install -y --no-install-recommends software-properties-common dirmngr gnupg apt-transport-https ca-certificates
apt-get clean && apt-get update
apt-get install -y ifupdown rsync nginx htop curl mc zip

echo "-------------"
echo " mariadb "
echo "-------------"
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirrors.dotsrc.org/mariadb/repo/10.3/debian stretch main'
apt update
export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< "mariadb-server mysql-server/root_password password vagrant"
sudo debconf-set-selections <<< "mariadb-server mysql-server/root_password_again password vagrant"
apt-get -y install mariadb-server mariadb-client

echo "-------------"
echo " nodejs 12.x "
echo "-------------"
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt-get install nodejs -y

echo "-------------"
echo " prepare project "
echo "-------------"

cd /vagrant
cp etc/mysql/conf.d/* /etc/mysql/conf.d
cp etc/dashboardConf/.env /vagrant/dashboard/
cp etc/dashboardServerConf/.env /vagrant/dashboardServer/

echo "-------------"
echo " create databases "
echo "-------------"
mysql -uroot -pvagrant -e "create database eltern_app DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_520_ci"
mysql -uroot -pvagrant -e "create user 'hortpro_dev'@'localhost' IDENTIFIED BY 'hortpro_dev';"
mysql -uroot -pvagrant -e "GRANT ALL PRIVILEGES ON eltern_app.* TO 'hortpro_dev'@'localhost';"

echo "-------------"
echo " set timezone "
echo "-------------"
sudo timedatectl set-timezone Europe/Berlin

echo "-------------"
echo " setup start directory "
echo "-------------"
if ! grep -q "cd /vagrant" /home/vagrant/.bashrc ; then
    echo "cd /vagrant" >> /home/vagrant/.bashrc
fi

echo "-------------"
echo " finished "
echo "-------------"