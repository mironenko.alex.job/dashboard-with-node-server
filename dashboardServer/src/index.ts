import { Server } from "@hapi/hapi";
import { Connection as DatabaseConnection } from "typeorm";
import JWTplugin from "hapi-auth-jwt2";
import Database from "./service/db";
import ApiAutoloader from "./plugins/API";

require('dotenv').config();

/**
 * Augment server.app type
 * @see https://github.com/DefinitelyTyped/DefinitelyTyped/issues/33809#issuecomment-472103564
 */
declare module "@hapi/hapi" {
  interface ServerApplicationState {
    db: DatabaseConnection;
  }
}

(async () => {
  try {
    const server = new Server({
      host: process.env.HOST,
      port: process.env.PORT,
      routes:{
        cors: {
          origin: ['*'],
          // additionalHeaders: ['token']
        }
      }
    });

    await server.register([
          { plugin: Database },
          { plugin: JWTplugin },
          { plugin: ApiAutoloader },
    ]);
    await server.start();

    if (server.info) {
      console.log(`web server started @ ${server.info.uri}`);
    }
  } catch (error) {
    console.error(error);
  }
})();
