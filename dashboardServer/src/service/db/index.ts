import { Server } from "@hapi/hapi";
import connect from "./database";

const Database = {
  name: "db",
  version: "1.0.0",
  register: async (server: Server) => {
    try {
      server.app.db = await connect();
      console.log("database connection successfully established...");
    } catch (e) {
      console.error(e);
      process.exit(1);
    }
  }
};

export default Database;
