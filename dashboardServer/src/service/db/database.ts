import "reflect-metadata";
import { createConnection } from "typeorm";

const connect = async () => {
  try {
    return await createConnection();
  } catch (e) {
    console.error(e);
    throw e;
  }
};

export default connect;
