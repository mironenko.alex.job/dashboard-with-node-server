import axios from "axios";

export function makeGetRequest(url:string) {
    return axios.get(url);
}

export function makePostRequest(url:string, options: any) {
    return axios
        .post(url, options)
        .then(function(resp) {
            return resp;
        })
        .catch(function(error) {
            throw new Error(error.message);
        });
}

export function makeArrayRequests(requests: any, prepareResponseData?:(resp: any) => [any]) {
    return axios.all(requests)
        .then(axios.spread((...responses: any) => {
            if (prepareResponseData){
                return prepareResponseData(responses);
            }
            return [];
        }));
}