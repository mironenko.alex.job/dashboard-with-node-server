import { Request, ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";
import axios from "axios";

import { makeArrayRequests, makeGetRequest } from "../../helpers/axiosHelper";
import { RequestWithUser } from "./interfaces";
import KidModel from "../../orm/entity/Kid";
import User from "../../orm/entity/User";
import Institution from "../../orm/entity/Institution";

export function prepareKidResponse(resp: [any]) {
    const kids:any = [];
    resp.forEach((response) => {
        const { data } = response;
        if (!data.message) {
            kids.push(data);
        }
    })
    return kids;
}

export function getKidPresences(kidId:string, schoolUrl: string) {
    return axios
        .get(`${schoolUrl}/service/kid/getPresences/${kidId}`, )
        .then(function(resp) {
            if (resp.data.success === false) {
                throw new Error('Wrong Kid Token. Loading Kid information failed');
            }
            return resp.data;
        })
        .catch(function(error) {
            throw new Error(error.message);
        });
}

export function getKidEvents(kidId:string, schoolUrl: string, date: string) {
    return axios
        .get(`${schoolUrl}/service/kid/getEvents/${kidId}/${date}`, )
        .then(function(resp) {
            if (resp.data.success === false) {
                throw new Error('Wrong Kid Token. Loading Kid information failed');
            }
            return resp.data;
        })
        .catch(function(error) {
            throw new Error(error.message);
        });
}

export function getKidInfo(kidId:string, schoolUrl: string) {
    return axios
        .get(`${schoolUrl}/service/kid/getInfo/${kidId}`, )
        .then(function(resp) {
            if (resp.data.success === false) {
                throw new Error('Wrong Kid Token.');
            }
            return resp;
        })
        .catch(function(error) {
            throw new Error(error.message);
        });
}

export async function getInstitution(schoolId: string) {
    try {
        const institution = await Institution.getByInstitutionToken(schoolId);
        return institution;
    }catch (error) {
        throw new Error('Wrong Institution Token');
    }
}

export async function getKidOrFail(kidId: string, schoolUrl: string) {
    try {
        const kid = await getKidInfo(kidId, schoolUrl);
        return kid;
    }catch (error) {
        throw new Error(error.message);
    }
}

const apis:Array<object> = [
    {
        method: 'POST',
        path: '/eltern-app/kid',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:RequestWithUser, h:ResponseToolkit) {
            try{
                const { user } = request;
                const body: any = request.payload;
                const { kidId, schoolId } = body;
                const institution = await getInstitution(schoolId);
                if (user && institution && kidId) {
                    await getKidOrFail(kidId, institution.url);
                    const kidRepository = request.server.app.db.getRepository(KidModel);
                    const Kid = new KidModel();
                    Kid.kid_id = kidId;
                    // @ts-ignore
                    Kid.user = user;
                    Kid.institution = institution;
                    await kidRepository.save(Kid);
                    const resp = await getKidInfo(kidId, institution.url);
                    return h.response({kid: resp.data}).code(200);

                }
            }
            catch (error) {
                console.error(error.message);
                return Boom.badRequest(error.message);
            }
        }
    },
    {
        method: 'GET',
        path: '/eltern-app/kids',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:RequestWithUser, h:ResponseToolkit) {
            try{
                const { user } = request;
                const kidRepository = request.server.app.db.getRepository(KidModel);
                const kids = await kidRepository.find(
                    {
                        where:{user},
                        relations: ["institution"],
                        select:['kid_id']
                    });
                const requests = kids.map((kid) => {
                    const url = `${kid.institution.url}/service/kid/getInfo/${kid.kid_id}`;
                    return makeGetRequest(url);
                })
                const kidsInfo = await makeArrayRequests(requests, prepareKidResponse);
                return h.response({kids: kidsInfo}).code(200);
            }
            catch (error) {
                return Boom.notFound(error.message);
            }
        }
    },
    {
        method: 'GET',
        path: '/eltern-app/kid/{kidId}/presences',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:RequestWithUser, h:ResponseToolkit) {
            try{
                const {user} = request;
                const {kidId} = request.params;
                const kidRepository = request.server.app.db.getRepository(KidModel);
                const kid = await kidRepository.findOneOrFail(
                    {
                        where:{user, kid_id: kidId},
                        relations: ["institution"],
                    });
                const kidPresences = await getKidPresences(kidId, kid.institution.url);
                return h.response({presences: kidPresences}).code(200);
            }
            catch (error) {
                // console.error(error.message);
                return Boom.badRequest(error.message);
            }
        }
    },
    {
        method: 'GET',
        path: '/eltern-app/kid/getEvents/{kidId}/{date}',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const authToken = request.headers.authorization;

                const {kidId, date} = request.params;

                if (authToken) {
                    const user = await User.getUserByToken(authToken.toString());

                    if (user) {
                        const kidRepository = request.server.app.db.getRepository(KidModel);
                        const kid = await kidRepository.findOneOrFail(
                            {
                                where:{user, kid_id: kidId},
                                relations: ["institution"],
                            });
                        const kidEvents = await getKidEvents(kidId, kid.institution.url, date);

                        return h.response({events: kidEvents, date: date}).code(200);
                    }
                    return h.response({error: 'User not found. Authentication required.'}).code(404);

                }
                return h.response({error: 'Authentication failed.'}).code(401);

            }
            catch (error) {
                // console.error(error.message);
                return h.response({error: error.message}).code(500);
            }
        }
    },
];

export default apis;