import { Request, ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import User from "../../orm/entity/User";
import Admin from "../../orm/entity/Admin";
import UserVerification from "../../orm/entity/UserVerification";

const apis:Array<object> = [
    {
        method: 'POST',
        path: '/eltern-app/user/login',
        options: {
            auth: {
                mode: 'optional'
            }
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const userRepository = request.server.app.db.getRepository(User);
                const body:any = request.payload;
                const userModel = new User();
                const user = await User.getUserByEmail(body.email);
                const verification = await UserVerification.getVerificationByUserId(user.id);
                const isAccountVerified = verification ? verification.is_verified : false;
                return bcrypt.compare(body.pwd, user.password).then(function(result) {
                    if (result) {
                        const token = jwt.sign({ id: user.email }, 'SUPER_SECRET_TOKEN'); // TODO create config and set secret
                        userModel.auth_token = token;
                        userRepository.update(user.id, userModel);
                        const resp = {
                            name: user.name,
                            last_name: user.last_name,
                            email: user.email,
                            authToken: token,
                            isAccountVerified,
                        }

                        return h.response({user: resp}).code(200)
                    }
                    throw Boom.badRequest('Wrong password', 'ElternApp');
                }).catch((error) => {
                    throw Boom.badRequest('Wrong password', 'ElternApp');
                });
            }
            catch (error) {
                const body:any = request.payload;
                throw Boom.notFound(`Account with email "${body.email}" does not exist`, 'ElternApp');
            }
        }
    },{
        method: 'POST',
        path: '/login',
        options: {
            auth: false
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const adminRepository = request.server.app.db.getRepository(Admin);
                const body:any = request.payload;
                const adminModel = new Admin();
                const admin = await Admin.getAdminByEmail(body.email);
                if (admin) {
                    return bcrypt.compare(body.pwd, admin.password).then(function(result) {
                        if (result) {
                            const token = jwt.sign(
                                { id: admin.id },
                                'SUPER_SECRET_DASHBOARD_TOKEN', // TODO create config and set secret
                                { expiresIn: 60 * 60 });
                            const resp = {
                                name: admin.name,
                                lastName: admin.last_name,
                                email: admin.email,
                                authToken: token,
                            }

                            return h.response({admin: resp}).code(200);
                        }
                        return Boom.badRequest('Wrong password');
                    }).catch((error) => {
                        throw Boom.badRequest('Wrong password');
                    });
                } 
                return Boom.notFound(`Account with email "${body.email}" does not exist`);
                
            }
            catch (error) {
                const body:any = request.payload;
                throw Boom.notFound(`Account with email "${body.email}" does not exist`);
            }
        }
    },
];

export default apis;