import {Request, ResponseToolkit} from "@hapi/hapi";
import bcrypt from "bcrypt";

import { RequestWithAdminId } from "./interfaces";
import Admin from "../../orm/entity/Admin";

const apis:Array<object> = [
    {
        method: 'GET',
        path: '/admins',
        options: {
            auth: 'jwt'
        },
        async handler (request:RequestWithAdminId, h:ResponseToolkit) {
            try{
                const { adminId } = request;
                const adminRepository = request.server.app.db.getRepository(Admin);
                const admins = await adminRepository.createQueryBuilder("admin")
                    .select(["admin.id", "admin.name", "admin.last_name", "admin.email", "admin.updated_at"])
                    .where("admin.id != :id", { id: adminId })
                    .getMany();
                return h.response({
                    admins
                }).code(200)
            }
            catch (error) {
                return console.error(error);
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                return await Admin.getAdminById(request.params.id);
            }
            catch (error) {
                console.error(error);
            }
        }
    },
    {
        method: 'POST',
        path: '/admin',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const adminRepository = request.server.app.db.getRepository(Admin);
                const body:any = request.payload;

                const admin = new Admin();
                const hash = await bcrypt.hashSync(body.pwd, 10);
                admin.name = body.name;
                admin.last_name = body.lastName;
                admin.password = hash;
                admin.email = body.email;

                await adminRepository.save(admin);
                return 'success';
            }
            catch (error) {
                console.error(error);
            }
        }
    },
    {
        method: 'PUT',
        path: '/admin/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const adminRepository = request.server.app.db.getRepository(Admin);
                const body:any = request.payload;
                const admin = new Admin();

                admin.name = body.name;
                admin.last_name = body.lastName;
                admin.email = body.email;

                await adminRepository.update(request.params.id, admin);
                return 'success';
            }
            catch (error) {
                console.error(error);
            }
        }
    },
    {
        method: 'DELETE',
        path: '/admin/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const adminRepository = request.server.app.db.getRepository(Admin);
                const admin = await Admin.getAdminById(request.params.id);
                await adminRepository.remove(admin);
                return 'DELETED';
            }
            catch (error) {
                console.error(error);
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/auth-check',
        options: {
            auth: 'jwt'
        },
        async handler (request:RequestWithAdminId, h:ResponseToolkit) {
            try{
                const adminRepository = request.server.app.db.getRepository(Admin);
                let admin = null;
                if (request.adminId) {
                    admin = await Admin.getAdmin(request.adminId);
                }
                return h.response({
                    admin
                }).code(200)
            }
            catch (error) {
                console.error(error);
            }
        }
    },
];

export default apis;