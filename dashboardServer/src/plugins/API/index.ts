import Boom from "@hapi/boom";
import { Server } from "@hapi/hapi";
import fs from 'fs';
import path from 'path';

import Admin from '../../orm/entity/Admin';
import root from './schemes/root';
import elternAppScheme from './schemes/elternApp';

const basename = path.basename(__filename);

const validate = async (decoded:any, req:any) => {
    const admin = await Admin.getRepository().findOne({where:{ id: decoded.id }});
    if (admin) {
        req.adminId = admin.id;
        return { isValid: true };
    }
    return { isValid: false };
};

const ApiAutoloader = {
    name: 'api',
    version: '1.0.0',
    register (server: Server) {
        server.auth.strategy('jwt', 'jwt', {
            key: 'SUPER_SECRET_DASHBOARD_TOKEN',
            validate,
            verifyOptions: { algorithms: ['HS256'] }
        });
        server.auth.scheme('RootScheme', root);
        server.auth.scheme('ElternAppScheme', elternAppScheme);

        server.auth.strategy('Default', 'RootScheme');
        server.auth.strategy('ElternApp', 'ElternAppScheme');
        server.auth.default('Default');

        let apis:any = [];

        fs
            .readdirSync(__dirname)
            .filter(file => {
                return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
            })
            .forEach(file => {
                const entityApis = require(path.join(__dirname, file));
                apis = apis.concat(entityApis.default);
            });


        server.route(apis);

        server.route({
            method: 'GET',
            path: '/',
            options: {
                auth: 'ElternApp'
            },
            handler: (request, h) => {
                console.log('request', request);
                return 'Not implemented';
            }
        });
    }
};

export default ApiAutoloader;