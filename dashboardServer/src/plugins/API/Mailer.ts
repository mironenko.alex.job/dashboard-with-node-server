// eslint-disable-next-line unicorn/filename-case
import {ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";
import NodeMailer from "nodemailer";
import moment from "moment";

import { RequestWithUser } from "./interfaces";
import UserVerification from "../../orm/entity/UserVerification";

interface EmailOptions {
    from: string,
    to:string,
    subject: string,
    html: string
}
const {env} = process;

const transporter = NodeMailer.createTransport({
    service: env.MAIL_SERVICE,
    auth: {
        user: env.MAIL_USER,
        pass: env.MAIL_PWD,
    }
});

const getEmailOptions = (userEmail: string, code: string):EmailOptions => {
    return {
        from: env.MAIL_ACC_FROM || 'dashboard@support.de', // sender address
        to: userEmail, // list of receivers
        subject: env.MAIL_SUBJECT || 'Account verification', // Subject line
        html: `<p>To verify your account please use code: ${code}</p>`// plain text body
    };
}

const apis:Array<object> = [
    {
        method: 'POST',
        path: '/eltern-app/send-verification-code',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:RequestWithUser, h:ResponseToolkit) {
            try{
                const { user }  = request;
                if (user) {
                    const { id, email } = user;
                    const code = Math.random().toString(36).slice(2, 7).toUpperCase();
                    const mailOptions = getEmailOptions(email, code);
                    const mailSendingStatus = await transporter.sendMail(mailOptions).then((info) => {
                        return 'success';
                    }).catch((error) => {
                        throw new Error(error.message);
                    })
                    if (mailSendingStatus === 'success') {
                        const validToDate = new Date();
                        validToDate.setDate(validToDate.getDate() + 1);
                        let verification = await UserVerification.getVerificationByUserId(id);
                        if (!verification) {
                            verification = new UserVerification();
                        }
                        verification.is_verified = false;
                        verification.valid_to = validToDate;
                        verification.code = code;
                        // @ts-ignore
                        verification.user = user;
                        await verification.save();

                        return h.response({message: 'Code sent successfully'}).code(200);
                    }
                }
            }
            catch (error) {
                return Boom.internal('Code', 'Code sending error');
            }
        }
    },
    {
        method: 'POST',
        path: '/eltern-app/validate-verification-code',
        options: {
            auth: 'ElternApp'
        },
        async handler (request:RequestWithUser, h:ResponseToolkit) {
            try{
                const { user } = request;
                const body: any = request.payload;
                const { code } = body;
                if (user) {
                    const verification = await UserVerification.getVerification(code, user.id);
                    if (verification) {
                        if (moment().isSameOrBefore(moment(verification.valid_to))) {
                            verification.is_verified = true;
                            await verification.save();
                            return h.response({message: 'Account verified successfully'}).code(200);
                        }
                        return Boom.badRequest('Code is expired');
                    }
                    return Boom.badRequest('Codes are not equal');
                }
                return Boom.notFound('User not found');
            }
            catch (error) {
                return Boom.internal('Code', 'Code sending error');
            }
        }
    },
];

export default apis;