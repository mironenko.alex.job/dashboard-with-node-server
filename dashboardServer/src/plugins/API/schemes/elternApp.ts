import { Request, ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";

import User from "../../../orm/entity/User";
import { RequestWithUser } from "../interfaces";

const elternAppScheme = () => {

    return {
        authenticate: async (request:RequestWithUser, h:ResponseToolkit) => {

            const authToken = request.headers.authorization;
            if (authToken) {
                try {
                    const user = await User.getUserByToken(authToken.toString());
                    const authUser = {
                        id: user.id,
                        name: user.name,
                        email: user.email,
                        auth_token: user.auth_token,
                    }
                    request.user = authUser
                    return h.authenticated({ credentials: {} });
                }catch (error) {
                    return Boom.unauthorized('Token is invalid. Authentication required. Please try to login again.', 'ElternApp');
                }
            }
            // eslint-disable-next-line @typescript-eslint/no-throw-literal
            return Boom.unauthorized('Token is invalid. Authentication required. Please try to login again.', 'ElternApp');

        }
    };
};
export default elternAppScheme;