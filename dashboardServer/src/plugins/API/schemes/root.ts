import { Request, ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";

const root = () => {

    return {
        authenticate (request:Request, h:ResponseToolkit) {
            const authToken = request.headers.authorization;
            if (!authToken) {
                // eslint-disable-next-line @typescript-eslint/no-throw-literal
                throw Boom.unauthorized(null, 'Default');
            }
            return h.authenticated({ credentials: {} });
        }
    };
};
export default root;