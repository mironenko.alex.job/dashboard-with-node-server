import { Request } from "@hapi/hapi";

export interface RequestWithUser extends Request{
    user?: {
        id: string,
        name: string,
        email: string,
        auth_token: string,
    } | null
}

export interface RequestWithAdminId extends Request{
    adminId?: string | null
}