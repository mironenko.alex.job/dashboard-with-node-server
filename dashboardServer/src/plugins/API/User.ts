import { Request, ResponseToolkit } from "@hapi/hapi";
import Boom from "@hapi/boom";
import jwt from "jsonwebtoken";
import bcrypt from 'bcrypt';

import User from "../../orm/entity/User";
import KidModel from "../../orm/entity/Kid";
import {getKidEvents} from "./Kid";
import UserVerification from "../../orm/entity/UserVerification";

export async function createUserOrFail(request:Request, h:ResponseToolkit) {
    const userRepository = request.server.app.db.getRepository(User);
    const body:any = request.payload;
    const user = new User();
    const hash = await bcrypt.hashSync(body.pwd, 10);
    const token = jwt.sign({ id: body.email }, 'SUPER_SECRET_TOKEN'); // TODO create config and set secret
    user.name = body.name;
    user.last_name = body.lastName;
    user.password = hash;
    user.email = body.email;
    user.auth_token = token;

    await userRepository.save(user);
    return h.response({
        user: {
            name: body.name,
            email: body.email,
            authToken: token,
            isAccountVerified: false,
        }
    }).code(200)
}

const apis:Array<object> = [
    {
        method: 'GET',
        path: '/users',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const userRepository = request.server.app.db.getRepository(User);
                const users = await userRepository.createQueryBuilder("user")
                    .select(["user.id", "user.name", "user.last_name", "user.email", "user.updated_at"])
                    .getMany();
                return h.response({
                    users
                }).code(200)
            }
            catch (error) {
                return console.error(error);
            }
        }
    },
    {
        method: 'GET',
        path: '/user/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                return await User.getUserById(request.params.id);
            }
            catch (error) {
                console.error(error);
            }
        }
    },
    {
        method: 'POST',
        path: '/eltern-app/user',
        options: {
            auth: {
                mode: 'optional'
            }
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                return await createUserOrFail(request, h);
            }
            catch (error) {
                const body:any = request.payload;
                return Boom.notFound(`Account with email "${body.email}" already in use.`, 'ElternApp')
            }
        }
    },
    {
        method: 'POST',
        path: '/user',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                return await createUserOrFail(request, h);
            }
            catch (error) {
                // console.error(error);
                const body:any = request.payload;
                return Boom.notFound(`Account with email "${body.email}" already in use.`, 'ElternApp');
            }
        }
    },
    {
        method: 'PUT',
        path: '/user/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const userRepository = request.server.app.db.getRepository(User);
                const body:any = request.payload;
                const user = new User();

                user.name = body.name;
                user.last_name = body.lastName;
                user.email = body.email;
                if (body.pwd){
                    user.password = await bcrypt.hashSync(body.pwd, 10);
                }
                await userRepository.update(request.params.id, user);
                return 'success';
            }
            catch (error) {
                console.error(error);
                return Boom.notFound('Can not update user');
            }
        }
    },
    {
        method: 'DELETE',
        path: '/user/{id}',
        options: {
            auth: 'jwt'
        },
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const userRepository = request.server.app.db.getRepository(User);
                const user = await User.getUserById(request.params.id);
                await userRepository.remove(user);
                return 'DELETED';
            }
            catch (error) {
                // console.error(error);
                return h.response({error: 'Can not delete user'}).code(404);
            }
        }
    },
    {
        method: 'POST',
        path: '/eltern-app/user/changPass',
        options: {
            auth: 'ElternApp'
        },
        // eslint-disable-next-line consistent-return
        async handler (request:Request, h:ResponseToolkit) {
            try{
                const userRepository = request.server.app.db.getRepository(User);
                const authToken = request.headers.authorization;
                const body:any = request.payload;

                if (authToken) {
                    const user = await User.getUserByToken(authToken.toString());
                    const {pass} = body;
                   // console.log("New pass:", pass);
                    if (user) {
                        const token = jwt.sign({ id: user.email }, 'SUPER_SECRET_TOKEN');
                        const verification = await UserVerification.getVerificationByUserId(user.id);
                        const isAccountVerified = verification ? verification.is_verified : false;
                        const hash = await bcrypt.hashSync(pass, 10);
                        user.password = hash;
                        user.auth_token = token;

                        userRepository.update(user.id, user);

                        const resp = {
                            name: user.name,
                            last_name: user.last_name,
                            email: user.email,
                            authToken: token,
                            isAccountVerified,
                        }
                        return h.response({user: resp}).code(200)
                    }
                    return h.response({error: 'User not found. Authentication required.'}).code(404);
                }
            }
            catch (error) {
                // console.error(error);
                const body:any = request.payload;
                return h.response({error: `Account with email "${body.email}" already in use.`}).code(404);
            }
        }
    },
    // {
    //     method: 'POST',
    //     path: '/user/changEmail',
    //     // eslint-disable-next-line consistent-return
    //     async handler (request:Request, h:ResponseToolkit) {
    //         try{
    //             const userRepository = request.server.app.db.getRepository(User);
    //             const authToken = request.headers.authorization;
    //             const body:any = request.payload;
    //
    //             if (authToken) {
    //                 const user = await User.getUserByToken(authToken.toString());
    //                 const {email} = body;
    //                 console.log("New pass:", email);
    //                 if (user) {
    //                     const token = jwt.sign({ id: email }, 'SUPER_SECRET_TOKEN');
    //                     const verification = await UserVerification.getVerificationByUserId(user.id);
    //                     const isAccountVerified = verification ? verification.is_verified : false;
    //                     user.email = email;
    //                     user.auth_token = token;
    //                     console.log("User:", user);
    //
    //                     userRepository.update(user.id, user);
    //
    //                     const resp = {
    //                         name: user.name,
    //                         last_name: user.last_name,
    //                         email: user.email,
    //                         authToken: token,
    //                         isAccountVerified,
    //                     }
    //                     return h.response({user: resp}).code(200)
    //                 }
    //                 return h.response({error: 'User not found. Authentication required.'}).code(404);
    //             }
    //         }
    //         catch (error) {
    //             // console.error(error);
    //             const body:any = request.payload;
    //             return h.response({error: `Account with email "${body.email}" already in use.`}).code(404);
    //         }
    //     }
    // },

];

export default apis;