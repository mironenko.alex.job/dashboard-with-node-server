import { EntitySubscriberInterface, EventSubscriber } from "typeorm";

interface BaseEntity {
  id: number;
}

@EventSubscriber()
export class LogSubscriber implements EntitySubscriberInterface {
  afterLoad(entity: BaseEntity) {
    console.log(`Entity loaded ${entity.constructor.name}: ${entity.id}`);
  }
}
