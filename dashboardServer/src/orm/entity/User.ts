import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

interface ConstructorProps {
}

@Entity({ name: "users" })

export default class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id!: string;

  @Column({ type: "varchar", length: 255, nullable: false })
  public name!: string;

  @Column({ type: "varchar", length: 255, nullable: true })
  public last_name!: string;

  @Column({ type: "varchar", length: 255, nullable: false, unique: true })
  public email!: string;

  @Column({ type: "varchar", length: 255, nullable: false })
  public password!: string;

  @Column({ type: "varchar", length: 255, nullable: false })
  public auth_token!: string;

  @Column({ type: "varchar", length: 255, nullable: true })
  public device_id!: string;

  @CreateDateColumn()
  public created_at!: Date;

  @UpdateDateColumn()
  public updated_at!: Date;

  constructor(props?: ConstructorProps) {
    super();
    if (props) {
    }
  }

  public static getUserById = async (id: string) =>
      User.getRepository().findOneOrFail({
        where: { id },
  });

  public static getUserByToken = async (authToken: string) =>
      User.getRepository().findOneOrFail({
        where: { auth_token: authToken },
  });

  public static getUserByEmail = async (email: string) =>
      User.getRepository().findOneOrFail({
        where: { email },
      });
}
