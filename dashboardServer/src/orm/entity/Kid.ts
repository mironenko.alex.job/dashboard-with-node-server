import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import User from "./User";
import Institution from "./Institution";

interface ConstructorProps {

}

@Entity({ name: "kids" })
export default class Kid extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  private id!: number;

  @Column({ type: "varchar", length: 255, nullable: true })
  public kid_id!: string;

  @CreateDateColumn()
  private created_at!: Date;

  @UpdateDateColumn()
  private updated_at!: Date;

  @ManyToOne(type => User, { onDelete: 'CASCADE' })
  @JoinColumn()
  public user!: User

  @ManyToOne(type => Institution, { onDelete: 'CASCADE' })
  @JoinColumn()
  institution!: Institution

  constructor(props?: ConstructorProps) {
    super();
  }

  public static getKid = async (id: number) =>
      Kid.getRepository().find({
        where: { id },
  });
}
