import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import User from "./User";

interface ConstructorProps {
}

@Entity({ name: "userVerification" })

export default class UserVerification extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id!: string;

  @Column({ type: "varchar", length: 20, nullable: false })
  public code!: string;

  @Column({ type: "boolean", nullable: false, default: false })
  public is_verified!: boolean;

  @CreateDateColumn()
  public created_at!: Date;

  @Column()
  public valid_to!: Date;

  @UpdateDateColumn()
  public updated_at!: Date;

  @OneToOne(type => User, { onDelete: 'CASCADE' })
  @JoinColumn()
  public user!: User


  constructor(props?: ConstructorProps) {
    super();
    if (props) {
    }
  }

  public static getVerificationByUserId = async (id: string) =>
      UserVerification.getRepository().findOne({
        where: { userId: id },
  });

  public static getVerification = async (code: string, userId: string) =>
      UserVerification.getRepository().findOne({
        where: { code, userId, is_verified: false },
  });
}
