import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

interface ConstructorProps {

}

@Entity({ name: "institutions" })
export default class Institution extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  private id!: number;

  @Column({ type: "varchar", length: 255, nullable: false })
  public institution_id!: string;

  @Column({ type: "varchar", length: 255, nullable: true })
  public url!: string;

  @CreateDateColumn()
  private created_at!: Date;

  @UpdateDateColumn()
  private updated_at!: Date;

  constructor(props?: ConstructorProps) {
    super();
  }

  public static getInstitutionById = async (id: string) =>
      Institution.getRepository().findOneOrFail({
        where: { id },
  });

  public static getByInstitutionToken = async (institutionId: string) =>
      Institution.getRepository().findOneOrFail({
        where: { institution_id: institutionId },
  });
}
