import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

interface ConstructorProps {

}

@Entity({ name: "roles" })
export default class Role extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  private id!: number;

  @Column({ type: "varchar", length: 255, nullable: false })
  public role!: string;

  @CreateDateColumn()
  private created_at!: Date;

  @UpdateDateColumn()
  private updated_at!: Date;

  // @OneToMany(
  //   type => DeviceToLocation,
  //   deviceToLocation => deviceToLocation.device
  // )
  // public deviceToLocations!: DeviceToLocation[];

  constructor(props?: ConstructorProps) {
    super();
    // if (props) {
    //   this.user_lastname = props.user_lastname;
    //   this.key = props.key;
    // }
  }

  // public static getByInventoryId = async (inventoryId: string) =>
  //   Device.getRepository().findOneOrFail({
  //     where: { inventoryId },
  //     relations: ["deviceToLocations", "deviceToLocations.location"]
  //   });
}
