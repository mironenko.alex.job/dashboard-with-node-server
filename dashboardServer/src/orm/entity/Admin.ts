import {
  BaseEntity,
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

// import Roles from "./Roles";

interface ConstructorProps {

}

@Entity({ name: "admins" })
export default class Admin extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id!: number;

  @Column({ type: "varchar", length: 255, nullable: false })
  public name!: string;

  @Column({ type: "varchar", length: 255, nullable: true })
  public last_name!: string;

  @Column({ type: "varchar", length: 255, nullable: false, unique: true })
  public email!: string;

  @Column({ type: "varchar", length: 255, nullable: false })
  public password!: string;

  @CreateDateColumn()
  private created_at!: Date;

  @UpdateDateColumn()
  private updated_at!: Date;

  constructor(props?: ConstructorProps) {
    super();
  }

  public static getAdminById = async (id: string) =>
      Admin.getRepository().findOneOrFail({
        where: { id },
  });

  public static getAdminByEmail = async (email: string) =>
      Admin.getRepository().findOne({
        where: { email },
  });

  public static getAdmin = async (id: string) =>
      Admin.getRepository().createQueryBuilder("admin")
          .select(["admin.id", "admin.name", "admin.last_name", "admin.email"])
          .where({id})
          .getOne();
}
