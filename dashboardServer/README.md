from application root directory:

1. run "vagrant up"
2. connect to vagrant box run "vagrant ssh"
3. cd to dashboardServer app folder run "cd dashboardServer" and 
   run "npm install"
4. start dashboardServer app run "npm run dev"
5. open new terminal window and cd to dashboard app folder run "cd dashboard" 
   and run "npm install"
6. start dashboard app run "npm run dev"
7. (optional) in new terminal window run "vagrant rsync-auto" 
    from eltern-app/vagrant directory.
    This command watches all local directories of any rsync synced folders and automatically initiates an rsync transfer when changes are detected
