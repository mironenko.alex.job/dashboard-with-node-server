import produce from 'immer';

import {
    ADMIN_LOGOUT,
    LOGIN_ADMIN,
    LOGIN_ADMIN_ERROR,
    LOGIN_ADMIN_SUCCESS,
    VALIDATE_AUTH_TOKEN_SUCCESS
} from './constants';

// const token = localStorage.getItem('token');

export const initialState = {
    loading: false,
    error: null,
    isTokenVerified: false,
    authToken: null,
    name: null,
    lastName: null,
    email: null,
};

const authReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case ADMIN_LOGOUT:
                localStorage.removeItem('id_token');
                draft.loading = false;
                draft.error = null;
                draft.authToken = null;
                draft.email = null;
                draft.name = null;
                draft.lastName = null;
                break;
            case LOGIN_ADMIN:
                draft.loading = true;
                draft.error = null;
                break;
            case LOGIN_ADMIN_SUCCESS:
                localStorage.setItem('id_token', action.admin.authToken);
                draft.loading = false;
                draft.error = null;
                draft.authToken = action.admin.authToken;
                draft.email = action.admin.email;
                draft.name = action.admin.name;
                draft.lastName = action.admin.lastName;
                draft.isTokenVerified = true;
                break;
            case LOGIN_ADMIN_ERROR:
                draft.loading = false;
                draft.error = action.message;
                break;
            case VALIDATE_AUTH_TOKEN_SUCCESS:
                draft.isTokenVerified = true;
                draft.loading = false;
                draft.error = null;
                if (action.admin) {
                    draft.authToken = action.admin.authToken;
                    draft.email = action.admin.email;
                    draft.name = action.admin.name;
                    draft.lastName = action.admin.lastName;
                }
                break;
        }
    });

export default authReducer;
