import { all, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { VALIDATE_AUTH_TOKEN } from './constants';
import {
    validateAuthTokenSuccess,
} from './actions';

export function* validateToken(action:any) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/admin/auth-check`;

        const response = yield axios
            .get(url, {
                headers: {
                    Authorization: 'Bearer ' + action.token
                }
            })
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                const { message } = error.response.data;
                throw new Error(error.message);
            });
        const admin = response.data.admin || null;
        if (admin) {
            admin.authToken = action.token;
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + admin.authToken;
        }
        yield put(validateAuthTokenSuccess(admin));
    } catch (err) {
        if (err.message === 'Request failed with status code 401') {
            yield put(validateAuthTokenSuccess());
        }
        // yield put(repoLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* adminSaga() {
    yield all([
        takeLatest(VALIDATE_AUTH_TOKEN, validateToken),
    ]);
}
