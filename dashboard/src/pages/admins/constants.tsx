export const ADMIN_LOGOUT = 'pages/admins/ADMIN_LOGOUT';

export const LOGIN_ADMIN = 'pages/admins/LOGIN_ADMIN';
export const LOGIN_ADMIN_SUCCESS = 'pages/admins/LOGIN_ADMIN_SUCCESS';
export const LOGIN_ADMIN_ERROR = 'pages/admins/LOGIN_ADMIN_ERROR';

export const VALIDATE_AUTH_TOKEN = 'pages/admins/VALIDATE_AUTH_TOKEN';
export const VALIDATE_AUTH_TOKEN_SUCCESS = 'pages/admins/VALIDATE_AUTH_SUCCESS';
export const VALIDATE_AUTH_TOKEN_ERROR = 'pages/admins/VALIDATE_AUTH_ERROR';
