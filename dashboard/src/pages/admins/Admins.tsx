import React from 'react';
import { Switch, Route, withRouter } from 'react-router';

import AdminsList from './list/AdminsList';
import AdminNew from './new/AdminNew';

class Admins extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/app/admins" exact component={AdminsList} />
        <Route path="/app/admins/new" exact component={AdminNew} />
      </Switch>
    );
  }
}

// @ts-ignore
export default withRouter(Admins);
