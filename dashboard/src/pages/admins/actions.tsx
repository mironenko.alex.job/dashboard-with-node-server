import {
    ADMIN_LOGOUT,
    LOGIN_ADMIN,
    LOGIN_ADMIN_SUCCESS,
    LOGIN_ADMIN_ERROR,
    VALIDATE_AUTH_TOKEN,
    VALIDATE_AUTH_TOKEN_SUCCESS,
    VALIDATE_AUTH_TOKEN_ERROR,
} from './constants';

export interface adminObj {
    name: string,
    lastName: string,
    email: string,
}

export interface adminData extends adminObj{
    authToken: string,
}


/**
 *
 */
export function logoutAdmin() {
    return {
        type: ADMIN_LOGOUT,
    };
}

/**
 *
 */
export function loginAdmin(email: string, pwd: string) {
    return {
        type: LOGIN_ADMIN,
        email,
        pwd,
    };
}

export function loginAdminSuccess(admin:adminData) {
    return {
        type: LOGIN_ADMIN_SUCCESS,
        admin,
    };
}

export function loginAdminError(message:string) {
    return {
        type: LOGIN_ADMIN_ERROR,
        message,
    };
}

export function validateAuthToken(token:string) {
    return {
        type: VALIDATE_AUTH_TOKEN,
        token,
    };
}

export function validateAuthTokenSuccess(admin?:adminObj) {
    return {
        type: VALIDATE_AUTH_TOKEN_SUCCESS,
        admin,
    };
}

export function validateAuthTokenError(message: string) {
    return {
        type: VALIDATE_AUTH_TOKEN_ERROR,
        message,
    };
}