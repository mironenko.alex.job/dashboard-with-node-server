export const DELETE_ADMIN = 'pages/admins/list/DELETE_ADMIN';
export const DELETE_ADMIN_SUCCESS = 'pages/admins/list/DELETE_ADMIN_SUCCESS';
export const DELETE_ADMIN_ERROR = 'pages/admins/list/DELETE_ADMIN_ERROR';

export const LOAD_ADMINS_SUCCESS = 'pages/admins/list/LOAD_ADMINS_SUCCESS';
export const LOAD_ADMINS = 'pages/admins/list/LOAD_ADMINS';
export const LOAD_ADMINS_ERROR = 'pages/admins/list/LOAD_ADMINS_ERROR';
