import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import {
  Table,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';

import s from './AdminsList.module.scss';
import Widget from '../../../components/Widget';
import { deleteAdmin, loadAdmins } from './actions';
import { RESTART_ON_REMOUNT } from 'utils/constants';
import injectSaga from 'utils/injectSaga';
import saga from './saga';

interface AdminsListProps {
  admins: [any],
  isFetching: boolean,
  dispatch: Dispatch<any>,
  onLoadAdmins(): Dispatch,
  onDeleteAdmin(id: string): Dispatch,
}

interface AdminsListState {

}

class AdminsList extends React.Component<AdminsListProps, AdminsListState> {
  static propTypes = {
    dispatch: PropTypes.func,
    admins: PropTypes.array, // eslint-disable-line
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    admins: [],
  };

  static meta = {
    title: 'Admins list',
    description: 'About description',
  };

  componentDidMount() {
      this.props.onLoadAdmins();
  }

  formatDate = (str:string) => {
    return str.replace(/,.*$/,"")
  }

  render() {
    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>Admins</BreadcrumbItem>
        </Breadcrumb>
        <h1>Admins</h1>
        <Widget
          className="pb-0"
          title={
            <div>
              <div className="pull-right mt-n-xs">
                <Link to="/app/admins/new" className="btn btn-sm btn-inverse">
                  Create new
                </Link>
              </div>
              <h5 className="mt-0">
                Admins <span className="fw-semi-bold">List</span>
              </h5>
            </div>
          }
        >
          <div className="widget-table-overflow">
            <Table striped>
              <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Last Updated</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              {this.props.admins &&
              this.props.admins.map(admin => (
                <tr key={admin.id}>
                  <td>{admin.name}</td>
                  <td>{admin.email}</td>
                  <td>{this.formatDate(new Date(admin.updated_at).toLocaleString())}</td>
                  <td>
                    <Link to="#" className="btn btn-sm btn-inverse" onClick={(evt) => {
                      evt.preventDefault();
                      this.props.onDeleteAdmin(admin.id);
                    }}>
                      Delete
                    </Link>
                  </td>
                </tr>
              ))}
              {this.props.isFetching && (
                <tr>
                  <td colSpan={100}>Loading...</td>
                </tr>
              )}
              </tbody>
            </Table>
          </div>
        </Widget>
      </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    isFetching: state.admins.loading,
    admins: state.admins.data,
  };
}

function mapDispatchToProps(dispatch:Dispatch) {
  return {
    onDeleteAdmin: (id:string) => dispatch(deleteAdmin(id)),
    onLoadAdmins: () => dispatch(loadAdmins()),
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'AdminsList', saga, RESTART_ON_REMOUNT });

// export default connect(mapStateToProps)(AdminsList);
export default compose(
    withSaga,
    withConnect,
)(AdminsList);