import produce from 'immer';
import {
    DELETE_ADMIN,
    DELETE_ADMIN_SUCCESS,
    DELETE_ADMIN_ERROR,
    LOAD_ADMINS_SUCCESS,
    LOAD_ADMINS,
    LOAD_ADMINS_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
    loading: false,
    error: null,
    data: [],
};

/* eslint-disable default-case, no-param-reassign */
const adminsReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case LOAD_ADMINS:
                draft.loading = true;
                draft.error = null;
                draft.data = [];
                break;

            case LOAD_ADMINS_SUCCESS:
                draft.data = action.admins;
                draft.loading = false;
                draft.error = null;
                break;

            case LOAD_ADMINS_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
                
            case DELETE_ADMIN:
                draft.loading = true;
                draft.error = null;
                break;
                
            case DELETE_ADMIN_SUCCESS:
                draft.error = null;
                draft.loading = false;
                break;

            case DELETE_ADMIN_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
        }
    });

export default adminsReducer;
