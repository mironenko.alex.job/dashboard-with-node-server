import {
    DELETE_ADMIN,
    DELETE_ADMIN_SUCCESS,
    DELETE_ADMIN_ERROR,
    LOAD_ADMINS,
    LOAD_ADMINS_SUCCESS,
} from './constants'

/**
 * Delete admin by id
 *
 */
export function deleteAdmin(id: string) {
    return {
        type: DELETE_ADMIN,
        id,
    };
}

/**
 * Admin deleted successfully
 */
export function deleteAdminSuccess() {
    return {
        type: DELETE_ADMIN_SUCCESS,
    };
}

/**
 * Set admin deletion error
 */
export function deleteAdminError(error: string) {
    return {
        type: DELETE_ADMIN_ERROR,
        error,
    };
}

/**
 * Load Admins
 *
 */
export function loadAdmins() {
    return {
        type: LOAD_ADMINS,
    };
}

/**
 * Load Admins
 * @param Admins {array} Admins
 */
export function loadAdminsSuccess(admins: []) {
    return {
        type: LOAD_ADMINS_SUCCESS,
        admins,
    };
}