import { all, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { DELETE_ADMIN, LOAD_ADMINS } from './constants';
import {
    deleteAdminSuccess,
    deleteAdminError,
    loadAdmins,
    loadAdminsSuccess,
} from './actions';

export function* getAdmins() {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/admins`;

        const response = yield axios
            .get(url)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        yield put(loadAdminsSuccess(response.data.admins));
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

export function* removeAdmin(action:any) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/admin/${action.id}`;

        yield axios
            .delete(url)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        yield put(deleteAdminSuccess());
        yield put(loadAdmins());
    } catch (err) {
        console.log('err', err.message);
        yield put(deleteAdminError(err.message));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* adminSaga() {
    yield all([
        takeLatest(LOAD_ADMINS, getAdmins),
        takeLatest(DELETE_ADMIN, removeAdmin)
    ]);
}
