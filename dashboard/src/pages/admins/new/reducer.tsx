import produce from 'immer';
import { CREATE_ADMIN_SUCCESS, CREATE_ADMIN, CREATE_ADMIN_ERROR } from './constants';

interface initialStateStructure {
    loading: boolean,
    error: null | string,
    message: null | string,
    isCreated: boolean,
}

// The initial state
export const initialState:initialStateStructure = {
    loading: false,
    error: null,
    message: null,
    isCreated: false,
};

/* eslint-disable default-case, no-param-reassign */
const adminReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case CREATE_ADMIN:
                draft.loading = true;
                draft.error = null;
                draft.message = null;
                break;

            case CREATE_ADMIN_SUCCESS:
                draft.loading = false;
                draft.isCreated = true;
                draft.message = 'success';
                break;

            case CREATE_ADMIN_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
        }
    });

export default adminReducer;
