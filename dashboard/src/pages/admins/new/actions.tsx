import { CREATE_ADMIN, CREATE_ADMIN_SUCCESS } from './constants'

/**
 * Create new admin
 *
 */
export function createAdmin(admin:{}) {
    return {
        type: CREATE_ADMIN,
        admin,
    };
}

export function createAdminSuccess() {
    return {
        type: CREATE_ADMIN_SUCCESS,
    };
}