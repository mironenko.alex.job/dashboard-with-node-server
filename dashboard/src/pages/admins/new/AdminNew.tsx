import React from 'react';
import PropTypes from 'prop-types';
import {compose, Dispatch} from "redux";
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { connect } from 'react-redux';

import Widget from '../../../components/Widget';

import { createAdmin } from './actions';
import s from './AdminNew.module.scss';
import injectSaga from "utils/injectSaga";
import saga from "./saga";

interface AdminProps {
  dispatch : Dispatch<any>,
  message: string,
  isFetching: boolean,
}

interface AdminState {
  name: string,
  lastName: string,
  email: string,
  pwd: string,
}

class AdminNew extends React.Component<AdminProps, AdminState> {
  static propTypes = {
    dispatch: PropTypes.func,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new admin',
    description: 'About description',
  };

  constructor(props:AdminProps) {
    super(props);

    this.state = {
      name: '',
      lastName: '',
      email: '',
      pwd: '',
    };
  }

  changeName = (event:any) => {
    this.setState({name: event.target.value});
  }

  changeLastName = (event:any) => {
    this.setState({lastName: event.target.value});
  }

  changeEmail = (event:any) => {
    this.setState({email: event.target.value});
  }

  changePwd = (event:any) => {
    this.setState({pwd: event.target.value});
  }

  doCreateAdmin = (e:any) => {
    const { name, lastName, email, pwd } = this.state;
    e.preventDefault();
    this.props
        .dispatch(
            createAdmin({
              name: name,
              lastName: lastName,
              email: email,
              pwd: pwd,
            }),
        )
  }

  render() {
    console.log('this.props.message', this.props.message);
    return (
      <div className={s.root}>
         <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>New Admin</BreadcrumbItem>
        </Breadcrumb>
        <h1>Create new Admin</h1>
        <Row>
          <Col sm={6}>
            <Widget
              title={
                <span>
                  Add Admin <span className="fw-semi-bold">Form</span>
                </span>
              }
            >
              <Form onSubmit={this.doCreateAdmin}>
                {this.props.message && (
                  <Alert className="alert-sm" bsstyle="info">
                    {this.props.message}
                  </Alert>
                )}
                <FormGroup>
                  <Label for="input-title">Name</Label>
                  <Input
                    id="input-title"
                    type="text"
                    placeholder="Username"
                    value={this.state.name}
                    required
                    onChange={this.changeName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-title">Last name</Label>
                  <Input
                    id="input-last-name"
                    type="text"
                    placeholder="Last Name"
                    value={this.state.lastName}
                    required
                    onChange={this.changeLastName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-content">Email</Label>
                  <Input
                    id="input-content"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.email}
                    required
                    onChange={this.changeEmail}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-pwd">Password</Label>
                  <Input
                      id="input-pwd"
                      type="password"
                      placeholder="Password"
                      value={this.state.pwd}
                      required
                      onChange={this.changePwd}
                  />
                </FormGroup>
                <div className="d-flex justify-content-end">
                  <ButtonGroup>
                    <Link to="/app/admins" className="btn btn-inverse">Cancel</Link>
                    <Button color="danger" type="submit">
                      {this.props.isFetching ? 'Creating...' : 'Create'}
                    </Button>
                  </ButtonGroup>
                </div>
              </Form>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
      isFetching: state.adminNew.loading,
      error: state.adminNew.error,
      message: state.adminNew.message,
  };
}

const withConnect = connect(
    mapStateToProps,
    null,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'newAdminSaga', saga });

export default compose(
    withSaga,
    withConnect,
)(AdminNew);
