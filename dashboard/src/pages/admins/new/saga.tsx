import { put, takeLatest } from 'redux-saga/effects';
import { CREATE_ADMIN } from './constants'
import { createAdminSuccess } from './actions'
import axios from 'axios';

interface userObject {
    admin: object
}

export function* createUser(action:userObject) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/admin`;

        const response = yield axios
            .post(url, action.admin)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        console.log('response', response);
        yield put(createAdminSuccess());
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* createAdminSaga() {
    // @ts-ignore
    yield takeLatest(CREATE_ADMIN, createUser);
}
