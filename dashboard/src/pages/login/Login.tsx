import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { 
  Alert,
  Button, 
  FormGroup, 
  Row,
  Col,
} from 'reactstrap';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

import s from './Login.module.scss';
import Widget from '../../components/Widget';
import Footer from "../../components/Footer";
import { loginAdmin } from '../admins/actions';
// import jwt from 'jsonwebtoken';
// import config from '../../config'
import { RESTART_ON_REMOUNT } from 'utils/constants';
import injectSaga from 'utils/injectSaga';
import saga from './saga';
import {compose, Dispatch} from "redux";

interface LoginProps {
  onLogin(email:string, pwd:string): Dispatch,
  loading: boolean,
  error: string,
}

interface LoginState {
  login: string,
  password: string,
}

class Login extends React.Component<LoginProps, LoginState> {
  static propTypes = {
    dispatch: PropTypes.func,
    isAuthenticated: PropTypes.bool,
    loading: PropTypes.bool,
    location: PropTypes.any, // eslint-disable-line
    error: PropTypes.string,
  };

  static defaultProps = {
    isAuthenticated: false,
    loading: false,
    location: {},
    errorMessage: null,
  };

  static isAuthenticated(token: string) {
    // We check if app runs with backend mode
    // if (!config.isBackend && token) return true;
    if (!token) return;
    // const date = new Date().getTime() / 1000;
    // const data = jwt.decode(token);
    // return date < data.exp;
  }

  constructor(props:any) {
    super(props);

    this.state = {
      login: '',
      password: '',
    };
  }

  changeLogin = (event:any) => {
    this.setState({login: event.target.value});
  }

  changePassword = (event:any) => {
    this.setState({password: event.target.value});
  }

  doLogin = (e:any) => {
    const { onLogin } = this.props;
    e.preventDefault();
    onLogin(this.state.login, this.state.password);
  }

  render() {
    const { error } = this.props;
    // @ts-ignore
    const {from} = this.props.location.state || {
      from: {pathname: '/app'},
    };

    // @ts-ignore
    if (this.props.isAuthenticated) {
      // cant access login page while logged in
      return <Redirect to={from} />;
    }
    return (
        <div className={s.root}>
          <Row>
            <Col xs={{size: 10, offset: 1}} sm={{size: 6, offset: 3}} lg={{size:4, offset: 4}}>
              <p className="text-center">Dashboard</p>
              <Widget className={s.widget}>
                <h4 className="mt-0">Login</h4>
                <ValidatorForm
                    ref="LoginForm"
                    className="mt"
                    onSubmit={this.doLogin}
                >
                  {error && (
                    <Alert size="sm" color="danger">
                      {error}
                    </Alert>
                  )}
                  <FormGroup className="form-group">
                    <TextValidator
                        className={`no-border ${s.long}`}
                        value={this.state.login}
                        onChange={this.changeLogin}
                        type="text"
                        name="email"
                        placeholder="Email"
                        validators={['required', 'isEmail']}
                        errorMessages={['This field is required', 'Email is not valid']}
                    />
                  </FormGroup>
                  <FormGroup>
                    <TextValidator
                        className={`no-border ${s.long}`}
                        value={this.state.password}
                        onChange={this.changePassword}
                        type="password"
                        name="password"
                        placeholder="Password"
                        validators={['required']}
                        errorMessages={['This field is required']}
                    />
                  </FormGroup>
                  <div className="d-flex justify-content-end align-items-center">
                    <div>
                      <Button color="success" size="sm" type="submit">
                        {this.props.loading ? 'Loading...' : 'Login'}
                      </Button>
                    </div>
                  </div>
                </ValidatorForm>
              </Widget>
            </Col>
          </Row>
          <Footer className="text-center" />
        </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    loading: state.auth.loading,
    isAuthenticated: state.auth.isAuthenticated,
    error: state.auth.error,
  };
}

function mapDispatchToProps(dispatch:Dispatch) {
  return {
    onLogin: (email:string, pwd:string) => dispatch(loginAdmin(email, pwd)),
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'loginAdmin', saga, RESTART_ON_REMOUNT });

// export default connect(mapStateToProps)(AdminsList);
export default compose(
    withSaga,
    withConnect,
)(Login);
