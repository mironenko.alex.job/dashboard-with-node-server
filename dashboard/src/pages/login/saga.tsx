import { all, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { LOGIN_ADMIN } from '../admins/constants';
import {
    loginAdminSuccess,
    loginAdminError
} from '../admins/actions';

export function* doAdminLogin(action:any) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/login`;

        const response = yield axios
            .post(url, {
                email: action.email,
                pwd: action.pwd,
            })
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                const { message } = error.response.data;
                throw new Error(message);
            });
        const admin = response.data.admin;
        if (admin.authToken) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + admin.authToken;
        }
        yield put(loginAdminSuccess(response.data.admin));
    } catch (err) {
      console.log(err.message);
        yield put(loginAdminError(err.message));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* adminSaga() {
    yield all([
        takeLatest(LOGIN_ADMIN, doAdminLogin),
    ]);
}
