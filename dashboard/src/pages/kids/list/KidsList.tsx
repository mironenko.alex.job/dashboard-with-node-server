import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {compose, Dispatch} from 'redux';
import {
  Table,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import injectSaga from 'utils/injectSaga';

import s from './KidsList.module.scss';
import Widget from '../../../components/Widget';
import { loadKids } from './actions';
import saga from './saga';

interface KidsListProps {
  dispatch : Dispatch<any>,
  kids : [any],
  isFetching : boolean,
}

class KidsList extends React.Component<KidsListProps> {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    kids: PropTypes.array, // eslint-disable-line
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    kids: [],
  };

  static meta = {
    title: 'Admins list',
    description: 'About description',
  };

  componentDidMount() {
    if(process.env.NODE_ENV === "development") {
      this.props.dispatch(loadKids());
    }
  }

  formatDate = (str:string) => {
    return str.replace(/,.*$/,"")
  }

  render() {
    console.log('this.props.kids', this.props.kids);
    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>Kids</BreadcrumbItem>
        </Breadcrumb>
        <h1>Kids</h1>
        <Widget
          className="pb-0"
          title={
            <div>
              <div className="pull-right mt-n-xs">
                <Link to="/app/admins/new" className="btn btn-sm btn-inverse">
                  Create new
                </Link>
              </div>
              <h5 className="mt-0">
                Kids <span className="fw-semi-bold">List</span>
              </h5>
            </div>
          }
        >
          <div className="widget-table-overflow">
            <Table striped>
              <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Last Updated</th>
              </tr>
              </thead>
              <tbody>
              {this.props.kids &&
              this.props.kids.map(kid => (
                <tr key={kid.id}>
                  <td>{kid.username}</td>
                  <td>{kid.email}</td>
                  <td>{this.formatDate(new Date(parseInt(kid.updatedAt)).toLocaleString())}</td>
                </tr>
              ))}
              {this.props.isFetching && (
                <tr>
                  <td colSpan={100}>Loading...</td>
                </tr>
              )}
              </tbody>
            </Table>
          </div>
        </Widget>
      </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    // isFetching: state.admins.isFetching,
    kids: state.kids.data,
  };
}

const withConnect = connect(
    mapStateToProps,
    null,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'KidsList', saga });

// export default connect(mapStateToProps)(AdminsList);
export default compose(
    withSaga,
    withConnect,
)(KidsList);
