export const LOAD_KIDS_SUCCESS = 'pages/kids/list/LOAD_KIDS_SUCCESS';
export const LOAD_KIDS = 'pages/kids/list/LOAD_KIDS';
export const LOAD_KIDS_ERROR = 'pages/kids/list/LOAD_KIDS_ERROR';
