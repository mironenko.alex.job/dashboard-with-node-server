import produce from 'immer';
import { LOAD_KIDS_SUCCESS, LOAD_KIDS, LOAD_KIDS_ERROR } from './constants';

// The initial state of the App
export const initialState = {
    loading: false,
    error: false,
    data: [],
};

/* eslint-disable default-case, no-param-reassign */
const adminsReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case LOAD_KIDS:
                draft.loading = true;
                draft.error = false;
                draft.data = [];
                break;

            case LOAD_KIDS_SUCCESS:
                draft.data = action.repos;
                draft.loading = false;
                break;

            case LOAD_KIDS_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
        }
    });

export default adminsReducer;
