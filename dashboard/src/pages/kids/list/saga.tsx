import { put, takeLatest } from 'redux-saga/effects';
import { LOAD_KIDS } from './constants'
import axios from 'axios';


export function* getKids() {
    try {

        const url = `http://192.168.42.40:5000/kids`;

        const response = yield axios
            .get(url)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        // yield put(setUser(response.data.user));
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* kidsSaga() {
    yield takeLatest(LOAD_KIDS, getKids);
}
