import { LOAD_KIDS } from './constants'

/**
 * Load kids
 *
 */
export function loadKids() {
    return {
        type: LOAD_KIDS,
    };
}