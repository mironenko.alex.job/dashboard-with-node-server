import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { connect } from 'react-redux';
import {Dispatch} from "redux";

import Widget from '../../../components/Widget';

// import { createAdmin } from '../../../actions/admins';
import s from './KidNew.module.scss';


interface KidState {
  username: string,
  email: string,
}

interface KidProps {
  dispatch : Dispatch<any>,
  message : string | null,
  isFetching : boolean,
}

class KidNew extends React.Component<KidProps, KidState> {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new admin',
    description: 'About description',
  };

  constructor(props:KidProps) {
    super(props);

    this.state = {
      username: '',
      email: '',
    };
  }

  changeName = (event:any) => {
    this.setState({username: event.target.value});
  }

  changeEmail = (event:any) => {
    this.setState({email: event.target.value});
  }

  doCreateAdmin = (e:any) => {
    // this.props
    //   .dispatch(
    //       createAdmin({
    //       username: this.state.username,
    //       email: this.state.email,
    //     }),
    //   )
    //   .then(() =>
    //     this.setState({
    //       username: '',
    //       email: '',
    //     }),
    //   );
    e.preventDefault();
  }

  render() {
    return (
      <div className={s.root}>
         <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>New Admin</BreadcrumbItem>
        </Breadcrumb>
        <h1>Create new Admin</h1>
        <Row>
          <Col sm={6}>
            <Widget
              title={
                <span>
                  Add Admin <span className="fw-semi-bold">Form</span>
                </span>
              }
            >
              <Form onSubmit={this.doCreateAdmin}>
                {this.props.message && (
                  <Alert className="alert-sm" bsstyle="info">
                    {this.props.message}
                  </Alert>
                )}
                <FormGroup>
                  <Label for="input-title">Username</Label>
                  <Input
                    id="input-title"
                    type="text"
                    placeholder="Username"
                    value={this.state.username}
                    required
                    onChange={this.changeName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-content">Email</Label>
                  <Input
                    id="input-content"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.email}
                    required
                    onChange={this.changeEmail}
                  />
                </FormGroup>
                <div className="d-flex justify-content-end">
                  <ButtonGroup>
                    <Button color="default">Cancel</Button>
                    <Button color="danger" type="submit">
                      {this.props.isFetching ? 'Creating...' : 'Create'}
                    </Button>
                  </ButtonGroup>
                </div>
              </Form>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps() {
  return {
    // isFetching: state.admins.isFetching,
    // message: state.admins.message,
  };
}

// @ts-ignore
export default connect(mapStateToProps)(KidNew);
