import React from 'react';
import { Switch, Route, withRouter } from 'react-router';

import KidsList from './list/KidsList';

class Kids extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/app/kids" exact component={KidsList} />
        {/*<Route path="/app/admins/new" exact component={AdminNew} />*/}
      </Switch>
    );
  }
}

// @ts-ignore
export default withRouter(Kids);