import { put, takeLatest } from 'redux-saga/effects';
import { CREATE_USER } from './constants'
import { createUserSuccess } from './actions'
import axios from 'axios';

interface userObject {
    user: object
}

export function* createUser(action:userObject) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/user`;

        const response = yield axios
            .post(url, action.user)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        console.log('response', response);
        yield put(createUserSuccess());
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* createUserSaga() {
    // @ts-ignore
    yield takeLatest(CREATE_USER, createUser);
}
