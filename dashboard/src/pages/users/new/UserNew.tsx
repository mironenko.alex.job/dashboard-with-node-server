import React from 'react';
import PropTypes from 'prop-types';
import {compose, Dispatch} from "redux";

import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { connect } from 'react-redux';

import Widget from '../../../components/Widget';

import { createUser } from './actions';
import s from './UserNew.module.scss';
import injectSaga from "utils/injectSaga";
import saga from "./saga";

interface UserProps {
  dispatch : Dispatch<any>,
  message: string,
}

interface UserState {
  name: string,
  lastName: string,
  email: string,
  pwd: string,
}

class UserNew extends React.Component<UserProps, UserState> {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Create new User',
    description: 'About description',
  };

  constructor(props:UserProps) {
    super(props);

    this.state = {
      name: '',
      lastName: '',
      email: '',
      pwd: '',
    };
  }

  changeName = (event:any) => {
    this.setState({name: event.target.value});
  }

  changeLastName = (event:any) => {
    this.setState({lastName: event.target.value});
  }

  changeEmail = (event:any) => {
    this.setState({email: event.target.value});
  }

  changePwd = (event:any) => {
    this.setState({pwd: event.target.value});
  }

  doCreateUser = (e:any) => {
    const { name, lastName, email, pwd } = this.state;
    e.preventDefault();
    this.props
      .dispatch(
          createUser({
            name: name,
            lastName: lastName,
            email: email,
            pwd: pwd,
        }),
      )
  }

  render() {
    return (
      <div className={s.root}>
         <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>New User</BreadcrumbItem>
        </Breadcrumb>
        <h1>Create new User</h1>
        <Row>
          <Col sm={6}>
            <Widget
              title={
                <span>
                  Add User <span className="fw-semi-bold">Form</span>
                </span>
              }
            >
              <Form onSubmit={this.doCreateUser}>
                {this.props.message && (
                  <Alert className="alert-sm" bsstyle="info">
                    {this.props.message}
                  </Alert>
                )}
                <FormGroup>
                  <Label for="input-title">Name</Label>
                  <Input
                    id="input-title"
                    type="text"
                    placeholder="Name"
                    value={this.state.name}
                    required
                    onChange={this.changeName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-name">Last Name</Label>
                  <Input
                    id="input-name"
                    type="text"
                    placeholder="LastName"
                    value={this.state.lastName}
                    required
                    onChange={this.changeLastName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-content">Email</Label>
                  <Input
                    id="input-content"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.email}
                    required
                    onChange={this.changeEmail}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-pwd">Password</Label>
                  <Input
                      id="input-pwd"
                      type="password"
                      placeholder="Password"
                      value={this.state.pwd}
                      required
                      onChange={this.changePwd}
                  />
                </FormGroup>
                <div className="d-flex justify-content-end">
                  <ButtonGroup>
                    <Button color="default">Cancel</Button>
                    <Button color="danger" type="submit">
                      {/*{this.props.isFetching ? 'Creating...' : 'Create'}*/}
                      Create
                    </Button>
                  </ButtonGroup>
                </div>
              </Form>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

// function mapStateToProps(state:any) {
//   return {
    // isFetching: state.admins.isFetching,
    // message: state.admins.message,
//   };
// }

const withConnect = connect(
    null,
    null,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'newUserSaga', saga });

export default compose(
    withSaga,
    withConnect,
)(UserNew);
