import { CREATE_USER, CREATE_USER_SUCCESS } from './constants'

/**
 * Create new user
 *
 */
export function createUser(user:{}) {
    return {
        type: CREATE_USER,
        user,
    };
}

export function createUserSuccess() {
    return {
        type: CREATE_USER_SUCCESS,
    };
}