import produce from 'immer';
import { CREATE_USER_SUCCESS, CREATE_USER, CREATE_USER_ERROR } from './constants';

// The initial state of the App
export const initialState = {
    loading: false,
    error: false,
    isCreated: false,
};

/* eslint-disable default-case, no-param-reassign */
const adminReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case CREATE_USER:
                draft.loading = true;
                draft.error = false;
                break;

            case CREATE_USER_SUCCESS:
                draft.loading = false;
                draft.isCreated = true;
                break;

            case CREATE_USER_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
        }
    });

export default adminReducer;
