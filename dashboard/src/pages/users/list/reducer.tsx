import produce from 'immer';
import {
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR,
    LOAD_USERS_SUCCESS,
    LOAD_USERS,
    LOAD_USERS_ERROR,
} from './constants';

// The initial state of the Users
export const initialState = {
    loading: false,
    error: false,
    data: [],
};

/* eslint-disable default-case, no-param-reassign */
const adminsReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case LOAD_USERS:
                draft.loading = true;
                draft.error = false;
                draft.data = [];
                break;

            case LOAD_USERS_SUCCESS:
                draft.data = action.users;
                draft.loading = false;
                break;

            case LOAD_USERS_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;

            case DELETE_USER:
                draft.loading = true;
                draft.error = false;
                break;

            case DELETE_USER_SUCCESS:
                draft.error = false;
                draft.loading = false;
                break;
                
            case DELETE_USER_ERROR:
                draft.error = action.error;
                draft.loading = false;
                break;
        }
    });

export default adminsReducer;
