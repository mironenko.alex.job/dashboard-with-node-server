import {
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_ERROR,
    LOAD_USERS,
    LOAD_USERS_SUCCESS,
} from './constants'

/**
 * Delete user by id
 *
 */
export function deleteUser(id: string) {
    return {
        type: DELETE_USER,
        id,
    };
}

/**
 * User deleted successfully
 */
export function deleteUserSuccess() {
    return {
        type: DELETE_USER_SUCCESS,
    };
}

/**
 * Set user deletion error
 */
export function deleteUserError(error: string) {
    return {
        type: DELETE_USER_ERROR,
        error,
    };
}

/**
 * Load users
 *
 */
export function loadUsers() {
    return {
        type: LOAD_USERS,
    };
}

/**
 * Load users
 * @param users {array} users
 */
export function loadUsersSuccess(users: []) {
    return {
        type: LOAD_USERS_SUCCESS,
        users,
    };
}