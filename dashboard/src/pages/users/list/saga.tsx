import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { DELETE_USER, LOAD_USERS } from './constants';

import {
    deleteUserSuccess,
    deleteUserError,
    loadUsers,
    loadUsersSuccess,
} from './actions'

export function* getUsers() {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/users`;

        const response = yield axios
            .get(url)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        yield put(loadUsersSuccess(response.data.users));
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

export function* removeUser(action:any) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/user/${action.id}`;

        yield axios
            .delete(url)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        yield put(deleteUserSuccess());
        yield put(loadUsers());
    } catch (err) {
        console.log('err', err.message);
        yield put(deleteUserError(err.message));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* usersSaga() {
    yield takeLatest(LOAD_USERS, getUsers);
    yield takeLatest(DELETE_USER, removeUser);
}
