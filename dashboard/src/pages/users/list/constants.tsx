export const LOAD_USERS_SUCCESS = 'pages/users/list/LOAD_USERS_SUCCESS';
export const LOAD_USERS = 'pages/users/list/LOAD_USERS';
export const LOAD_USERS_ERROR = 'pages/users/list/LOAD_USERS_ERROR';

export const DELETE_USER = 'pages/users/list/DELETE_USER';
export const DELETE_USER_SUCCESS = 'pages/users/list/DELETE_USER_SUCCESS';
export const DELETE_USER_ERROR = 'pages/users/list/DELETE_USER_ERROR';
