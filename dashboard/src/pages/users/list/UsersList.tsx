import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Dispatch } from 'redux';
import {
  Table,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import injectSaga from 'utils/injectSaga';

import s from './UsersList.module.scss';
import Widget from '../../../components/Widget';
import { deleteUser, loadUsers } from './actions';
import saga from './saga';

interface UsersListProps {
  onLoadUsers() : Dispatch,
  onDeleteUser(id: string) : Dispatch,
  dispatch? : Dispatch<any>,
  users : [any],
  isFetching : boolean,
}

class UsersList extends React.Component<UsersListProps> {
  static propTypes = {
    dispatch: PropTypes.func,
    users: PropTypes.array, // eslint-disable-line
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    users: [],
  };

  static meta = {
    title: 'Users list',
    description: 'Users description',
  };

  componentDidMount() {
      this.props.onLoadUsers();
  }

  formatDate = (str: string) => {
    return str.replace(/,.*$/,"")
  }

  render() {
    const { onDeleteUser } = this.props;
    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>Users</BreadcrumbItem>
        </Breadcrumb>
        <h1>Users</h1>
        <Widget
          className="pb-0"
          title={
            <div>
              <div className="pull-right mt-n-xs">
                <Link to="/app/users/new" className="btn btn-sm btn-inverse">
                  Create new
                </Link>
              </div>
              <h5 className="mt-0">
                Users <span className="fw-semi-bold">List</span>
              </h5>
            </div>
          }
        >
          <div className="widget-table-overflow">
            <Table striped>
              <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Last Updated</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              {this.props.users &&
              this.props.users.map(user => (
                <tr key={user.id}>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{this.formatDate(new Date(user.updated_at).toLocaleString())}</td>
                  <td>
                    <Link
                        to={`/app/users/user/${user.id}/update`}
                        className="btn btn-sm btn-inverse"
                        style={{marginRight: 5}}
                    >
                      Update
                    </Link>
                    <Link to="#" className="btn btn-sm btn-inverse" onClick={(evt) => {
                      evt.preventDefault();
                      onDeleteUser(user.id);
                    }}>
                      Delete
                    </Link>
                  </td>
                </tr>
              ))}
              {this.props.isFetching && (
                <tr>
                  <td colSpan={100}>Loading...</td>
                </tr>
              )}
              </tbody>
            </Table>
          </div>
        </Widget>
      </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    isFetching: state.users.loading,
    users: state.users.data,
  };
}

function mapDispatchToProps(dispatch:Dispatch) {
  return {
    onDeleteUser: (id:string) => dispatch(deleteUser(id)),
    onLoadUsers: () => dispatch(loadUsers()),
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'UsersList', saga });

export default compose(
    withSaga,
    withConnect,
)(UsersList);
