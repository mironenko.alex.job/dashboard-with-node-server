import { UPDATE_USER, UPDATE_USER_REFRESH_STORE, UPDATE_USER_SUCCESS } from './constants'

export interface UserData {
    userId: string,
    name: string,
    lastName: string,
    email: string,
    pwd?:string,
}

/**
 * Update user by id
 *
 */
export function updateUser(user:UserData) {
    return {
        type: UPDATE_USER,
        user
    };
}

export function updateUserRefreshStore() {
    return {
        type: UPDATE_USER_REFRESH_STORE,
    };
}

export function updateUserSuccess() {
    return {
        type: UPDATE_USER_SUCCESS,
    };
}