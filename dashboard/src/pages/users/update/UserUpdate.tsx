import React from 'react';
import PropTypes from 'prop-types';
import {compose, Dispatch} from "redux";
import { Link } from 'react-router-dom';

import {
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  ButtonGroup,
  Alert,
  Label,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';
import { connect } from 'react-redux';

import { RESTART_ON_REMOUNT } from 'utils/constants';
import Widget from '../../../components/Widget';

import { updateUser, updateUserRefreshStore } from './actions';
import s from './UserUpdate.module.scss';
import injectSaga from "utils/injectSaga";
import saga from "./saga";

interface UserProps {
  users: [any],
  dispatch : Dispatch<any>,
  message: string,
  match: any,
  history: any,
  isFetching: boolean,
}

interface UserState {
  userId: string,
  name: string,
  lastName: string,
  email: string,
  newPwd?: string,
  newPwdRepeat?: string,
}

class UserUpdate extends React.Component<UserProps, UserState> {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    message: PropTypes.string,
    isFetching: PropTypes.bool,
  };

  static defaultProps = {
    isFetching: false,
    message: null,
  };

  static meta = {
    title: 'Update User',
    description: 'Update User description',
  };

  constructor(props:UserProps) {
    super(props);
    const { users } = props;
    const userId = props.match.params.id;
    const filteredUsers:any = users.filter((user) => user.id === userId);
    const user = (filteredUsers.length > 0) ? filteredUsers[0] : null;
    if (!user) {
      this.props.history.replace('/app/users');
    }
    const userState = {
      userId: '',
      name: '',
      lastName: '',
      email: '',
      newPwd: '',
      newPwdRepeat: '',
    };
    if (user) {
      userState.userId = user.id;
      userState.name = user.name;
      userState.lastName = user.last_name;
      userState.email = user.email;
    }

    this.state = userState;
  }

  changeName = (event:any) => {
    this.setState({name: event.target.value});
  }

  changeLastName = (event:any) => {
    this.setState({lastName: event.target.value});
  }

  changeEmail = (event:any) => {
    this.setState({email: event.target.value});
  }

  updatePwd = (event:any) => {
    this.setState({newPwd: event.target.value});
  }

  updatePwdRepeat = (event:any) => {
    this.setState({newPwdRepeat: event.target.value});
  }

  doUpdateUser = (e:any) => {
    const { userId, name, lastName, email, newPwd, newPwdRepeat } = this.state;
    e.preventDefault();
    const userData = {
      userId: userId,
      name: name,
      lastName: lastName,
      email: email,
    }
    if (newPwd && newPwd.length > 0 && newPwd === newPwdRepeat){
      // @ts-ignore
      userData.pwd = newPwd;
    }
    this.props
        .dispatch(
            updateUser(userData),
        )

  }

  componentWillUnmount(): void {
    this.props.dispatch(updateUserRefreshStore());
  }

  render() {
    return (
      <div className={s.root}>
         <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
          <BreadcrumbItem active>Update User</BreadcrumbItem>
        </Breadcrumb>
        <h1>Update User</h1>
        <Row>
          <Col sm={6}>
            <Widget
              title={
                <span>
                  Update User <span className="fw-semi-bold">Form</span>
                </span>
              }
            >
              <Form onSubmit={this.doUpdateUser}>
                {this.props.message && (
                  <Alert className="alert-sm" bsstyle="info">
                    {this.props.message}
                  </Alert>
                )}
                <FormGroup>
                  <Label for="input-title">Name</Label>
                  <Input
                    id="input-title"
                    type="text"
                    placeholder="Name"
                    value={this.state.name}
                    required
                    onChange={this.changeName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-name">Last Name</Label>
                  <Input
                    id="input-name"
                    type="text"
                    placeholder="LastName"
                    value={this.state.lastName}
                    required
                    onChange={this.changeLastName}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-content">Email</Label>
                  <Input
                    id="input-content"
                    className="form-control"
                    placeholder="Email"
                    value={this.state.email}
                    required
                    onChange={this.changeEmail}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-pwd">Change Password</Label>
                  <Input
                      id="input-pwd"
                      type="password"
                      placeholder="Password"
                      value={this.state.newPwd}
                      onChange={this.updatePwd}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="input-pwd">Repeat Password</Label>
                  <Input
                      id="input-pwd-repeat"
                      type="password"
                      placeholder="Repeat Password"
                      value={this.state.newPwdRepeat}
                      onChange={this.updatePwdRepeat}
                  />
                </FormGroup>
                <div className="d-flex justify-content-end">
                  <ButtonGroup>
                    <Link to="/app/users" className="btn btn-inverse">Cancel</Link>
                    <Button color="danger" type="submit">
                      {this.props.isFetching ? 'Updating...' : 'Update'}
                    </Button>
                  </ButtonGroup>
                </div>
              </Form>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    isFetching: state.userUpdate.loading,
    message: state.userUpdate.message,
    users: state.users.data,
  };
}

const withConnect = connect(
    mapStateToProps,
    null,
);

// @ts-ignore
const withSaga = injectSaga({ key: 'updateUserSaga', saga, RESTART_ON_REMOUNT });

export default compose(
    withSaga,
    withConnect,
)(UserUpdate);
