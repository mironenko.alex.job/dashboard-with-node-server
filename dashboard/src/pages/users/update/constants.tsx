export const UPDATE_USER = 'pages/users/update/UPDATE_USER';
export const UPDATE_USER_SUCCESS = 'pages/users/update/UPDATE_USER_SUCCESS';
export const UPDATE_USER_ERROR = 'pages/users/update/UPDATE_USER_ERROR';
export const UPDATE_USER_REFRESH_STORE = 'pages/users/update/UPDATE_USER_REFRESH_STORE';
