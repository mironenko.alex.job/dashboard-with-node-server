import produce from 'immer';
import {
    UPDATE_USER_SUCCESS,
    UPDATE_USER,
    UPDATE_USER_REFRESH_STORE,
    UPDATE_USER_ERROR,
} from './constants';

interface InitialTypes {
    loading: boolean,
    error: null | string,
    isUpdated: boolean,
    message: null | string,
}

// The initial state
export const initialState:InitialTypes = {
    loading: false,
    error: null,
    isUpdated: false,
    message: null,
};

/* eslint-disable default-case, no-param-reassign */
const updateUserReducer = (state = initialState, action: any) =>
    produce(state, (draft) => {
        switch (action.type) {
            case UPDATE_USER:
                draft.loading = true;
                draft.error = null;
                draft.message = null;
                break;

            case UPDATE_USER_SUCCESS:
                draft.loading = false;
                draft.isUpdated = true;
                draft.error = null;
                draft.message = 'success';
                break;
            case UPDATE_USER_REFRESH_STORE:
                draft.loading = false;
                draft.isUpdated = false;
                draft.error = null;
                draft.message = null;
                break;

            case UPDATE_USER_ERROR:
                draft.error = action.error;
                draft.loading = false;
                draft.error = 'error';
                break;
        }
    });

export default updateUserReducer;
