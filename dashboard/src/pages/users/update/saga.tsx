import { put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';

import { UPDATE_USER } from './constants'
import { updateUserSuccess, UserData } from './actions'

interface userObject {
    user: UserData
}

export function* updateUser(action:userObject) {
    try {

        const url = `${process.env.REACT_APP_API_PATH}/user/${action.user.userId}`;

        const response = yield axios
            .put(url, action.user)
            .then(function(resp) {
                return resp;
            })
            .catch(function(error) {
                throw new Error(error.message);
            });
        console.log('response', response);
        yield put(updateUserSuccess());
    } catch (err) {
        // yield put(repoLoadingError(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* updateUserSaga() {
    // @ts-ignore
    yield takeLatest(UPDATE_USER, updateUser);
}
