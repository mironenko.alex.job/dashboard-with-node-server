import React from 'react';
import { Switch, Route, withRouter } from 'react-router';

import UsersList from './list/UsersList';
import UserNew from './new/UserNew';
import UserUpdate from './update/UserUpdate';

class Users extends React.Component {
  render() {
    return (
      <Switch>
        <Route path="/app/users" exact component={UsersList} />
        <Route path="/app/users/new" exact component={UserNew} />
        <Route path="/app/users/user/:id/update" exact component={UserUpdate} />
      </Switch>
    );
  }
}

// @ts-ignore
export default withRouter(Users);
