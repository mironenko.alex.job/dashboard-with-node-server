import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';

// import { fetchPosts } from '../../actions/posts';
import s from './Dashboard.module.scss';

class Dashboard extends Component {
  /* eslint-disable */
  static propTypes = {
    posts: PropTypes.any,
    isFetching: PropTypes.bool,
    dispatch: PropTypes.func.isRequired,
  };
  /* eslint-enable */

  static defaultProps = {
    posts: [],
    isFetching: false,
  };

  state = {
    isDropdownOpened: false
  };

  componentDidMount() {
    if(process.env.NODE_ENV === "development") {
      // this.props.dispatch(fetchPosts());
    }
  }

  formatDate = (str:string) => {
    return str.replace(/,.*$/,"");
  }

  // toggleDropdown = () => {
  //   this.setState(prevState => ({
  //     isDropdownOpened: !prevState.isDropdownOpened,
  //   }));
  // }

  render() {
    return (
      <div className={s.root}>
        <Breadcrumb>
          <BreadcrumbItem>Dashboard</BreadcrumbItem>
        </Breadcrumb>
        <h1 className="mb-lg">Dashboard content</h1>

      </div>
    );
  }
}

function mapStateToProps() {
  return {
    // isFetching: state.posts.isFetching,
    // posts: state.posts.posts,
  };
}

export default connect(mapStateToProps)(Dashboard);
