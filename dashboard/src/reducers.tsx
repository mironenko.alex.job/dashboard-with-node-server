/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';

import auth from './pages/admins/reducer';
import adminList from './pages/admins/list/reducer';
import adminNew from './pages/admins/new/reducer';
import kidsList from './pages/kids/list/reducer';
import usersList from './pages/users/list/reducer';
import updateUser from './pages/users/update/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
    const rootReducer = combineReducers({
        auth,
        admins: adminList,
        adminNew: adminNew,
        kids: kidsList,
        users: usersList,
        userUpdate: updateUser,
        ...injectedReducers,
    });

    return rootReducer;
}
