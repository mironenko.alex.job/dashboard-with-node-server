import React from 'react';
import cx from 'classnames';
import PropTypes from 'prop-types';
import {
  Navbar,
  Nav,
  NavItem,
  Button,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';
import { NavLink, Redirect } from 'react-router-dom';
import {connect} from "react-redux";
import {compose, Dispatch} from "redux";

import Icon from '../Icon';
// @ts-ignore
import photo from '../../images/smile.png';
// import { logoutUser } from '../../actions/user';
import s from './Header.module.scss';
import { logoutAdmin } from "../../pages/admins/actions";
// import saga from "./saga";
// import injectSaga from "utils/injectSaga";
// import {RESTART_ON_REMOUNT} from "utils/constants";

interface HeaderProps {
  name: string;
  lastName: string;
  sidebarToggle(): void;
  onLogOut(): void;
}
interface HeaderState {
  isOpen: boolean;
}

class Header extends React.Component<HeaderProps, HeaderState> {
  static propTypes = {
    sidebarToggle: PropTypes.func,
    dispatch: PropTypes.func,
  };

  static defaultProps = {
    sidebarToggle: () => {},
  };

  state = { isOpen: false };

  toggleDropdown = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));
  }

  doLogout = () => {
    const { onLogOut } = this.props;
    onLogOut();
  }

  render() {
    const {name, lastName} = this.props;
    const {isOpen} = this.state;
    return (
      <Navbar className={s.root}>
        <Nav>
          <NavItem
            className={cx('visible-xs mr-4 d-sm-up-none', s.headerIcon, s.sidebarToggler)}
            href="#"
            onClick={this.props.sidebarToggle}
          >
            <i className="fa fa-bars fa-2x text-muted" />
          </NavItem>
          <NavItem>
            <InputGroup>
              <Input placeholder="Search for..." />
              <InputGroupAddon addonType="append" className="px-2">
                <i className="fa fa-search" />
              </InputGroupAddon>
            </InputGroup>
          </NavItem>
        </Nav>
        <Nav className="ml-auto">
          <NavItem className={cx('', s.headerIcon)}>
            <Button>
              <Icon glyph="mail"/>
              <span>8</span>
            </Button>
          </NavItem>
          <NavItem className={cx('', s.headerIcon)}>
            <Button>
              <Icon glyph="notification"/>
              <span>13</span>
            </Button>
          </NavItem>
          <NavItem className={cx('', s.headerIcon)}>
            <Button>
              <Icon glyph="settings"/>
            </Button>
          </NavItem>
          <Dropdown isOpen={isOpen} toggle={this.toggleDropdown}>
            <DropdownToggle nav>
              <img className={cx('rounded-circle mr-sm', s.adminPhoto)} src={photo} alt="administrator" />
              <span className="text-body">{`${name} ${lastName}`}</span>
              <i className={cx('fa fa-angle-down ml-sm', s.arrow, {[s.arrowActive]: isOpen})} />
            </DropdownToggle>
            <DropdownMenu style={{width: '100%'}}>
              {/*<DropdownItem>*/}
              {/*  <NavLink to="/app/profile">Profile</NavLink>*/}
              {/*</DropdownItem>*/}
              <DropdownItem onClick={this.doLogout}>
                Logout
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Nav>
      </Navbar>
    );
  }
}

function mapStateToProps(state:any) {
  return {
    name: state.auth.name,
    lastName: state.auth.lastName,
  };
}

function mapDispatchToProps(dispatch:Dispatch) {
  return {
    onLogOut: () => dispatch(logoutAdmin()),
  };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

// @ts-ignore
// const withSaga = injectSaga({ key: 'logOutAdmin', saga, RESTART_ON_REMOUNT });

// export default connect(mapStateToProps)(AdminsList);
export default compose(withConnect)(Header);
