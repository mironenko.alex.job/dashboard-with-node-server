import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import * as s from './Footer.module.scss';

interface FooterProps {
    className: string
}

class Footer extends React.Component<FooterProps> {
    static propTypes = {
        className: PropTypes.string,
    };

    static defaultProps = {
        className: '',
    };

    render(): JSX.Element {
        return (
            <footer className={cx(s.root, this.props.className)}>
                <div className={s.container}>
                    <span>© 2020 &nbsp;HortPro </span>
                    <span className={s.spacer}>·</span>
                    <Link to="/app/not-found">Support</Link>
                </div>
            </footer>
        );
    }
}

export default Footer;
