import React from 'react';
import {withRouter, Link} from 'react-router-dom';

import Icon from '../Icon';
import LinksGroup from './LinksGroup/LinksGroup';

import s from './Sidebar.module.scss';

const Sidebar = () => (
  <nav className={s.root}>
    <header className={s.logo}>
      <Link to="/app/main">
        <Icon glyph="hortproLogo" />
      </Link>
    </header>
    <ul className={s.nav}>
      <LinksGroup
        header="Admins"
        headerLink="/app/admins"
        glyph="admins"
      />
        <LinksGroup
            header="Users"
            headerLink="/app/users"
            glyph="admins"
        />
    </ul>
  </nav>
);

export default withRouter(Sidebar);
