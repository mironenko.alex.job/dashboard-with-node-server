import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router';
import { HashRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { Dispatch } from 'redux';
import axios  from 'axios';

import ErrorPage from '../pages/error';
import LayoutComponent from '../components/Layout';
import Loading from '../components/Loading';
import Login from '../pages/login';
// import Register from '../pages/register';
// import { logoutUser } from '../actions/user';
import { validateAuthToken, validateAuthTokenSuccess, logoutAdmin } from '../pages/admins/actions';
import '../styles/theme.scss';

interface RouteProps {
    email: null | string,
    path: string,
    dispatch : Dispatch<any>,
    component: React.ComponentClass,
    rest?: any,
}

interface appProps {
    email: null | string,
    isTokenVerified: boolean,
    dispatch : Dispatch<any>,
}



const PublicRoute = (props: RouteProps) => {
    const { email, component, rest } = props;
    if (email) {
        return <Redirect to="/app/main" />;

    } else {
        return (
            <Route {...rest} render={(properties:any) => React.createElement(component, properties)} />
        );
    }
};

const PrivateRoute = (props: RouteProps) => {
    const { email, dispatch, component, rest } = props;
    if (!email) {
        // dispatch(logoutUser());
        return <Redirect to="/login" />;
    } else {
        return ( // eslint-disable-line
            <Route {...rest} render={(properties:any) => React.createElement(component, properties)} />
        );
    }
};

// eslint-disable-next-line react/prop-types
const CloseButton = ({ closeToast }:{ closeToast?(): void }) => <i onClick={closeToast} className="la la-close notifications-close" />;

class App extends React.PureComponent<appProps, {}> {
    componentDidMount(): void {
        const { dispatch, isTokenVerified } = this.props;
        this.initUnauthorizedErrorInterceptor();
        const token = localStorage.getItem('id_token');
        if (!isTokenVerified && token) {
            dispatch(validateAuthToken(token));
        }else {
            dispatch(validateAuthTokenSuccess());
        }
    }

    initUnauthorizedErrorInterceptor = () => {
        const { dispatch } = this.props;
        // Add a response interceptor
        axios.interceptors.response.use(function (response) {
            // Do something with response data
            return response;
        }, function (error) {
            // Do something with response error
            if (error.response && error.response.status === 401){
                dispatch(logoutAdmin());
            }
            return Promise.reject(error);
        });
    }

    render() {
        const { dispatch, isTokenVerified, email } = this.props;
        let content = <Loading />;
        if (isTokenVerified) {
            content = (<>
                <ToastContainer autoClose={5000} hideProgressBar closeButton={<CloseButton />} />
                <HashRouter>
                    <Switch>
                        <Route path="/" exact render={() => <Redirect to="/app/main" />} />
                        <Route path="/app" exact render={() => <Redirect to="/app/main  " />} />
                        <PublicRoute
                            path="/login"
                            email={email}
                            dispatch={dispatch}
                            component={Login}
                        />
                        <PrivateRoute
                            path="/app"
                            email={email}
                            dispatch={dispatch}
                            component={LayoutComponent}
                        />
                        {/*<Route path="/register" exact component={Register}/>*/}
                        {/*<Route path="/login" exact component={Login}/>*/}
                        <Route path="/error" exact component={ErrorPage} />
                    </Switch>
                </HashRouter>
            </>);
        }
        return (content);
    }
}

// const mapStateToProps = (state: any) => ({
//     isTokenVerified: state.auth.isTokenVerified,
// });

function mapStateToProps(state:any) {
    return {
        isTokenVerified: state.auth.isTokenVerified,
        email: state.auth.email,
    };
}

export default connect(mapStateToProps)(App);
