import React from 'react';
import cx from 'classnames';
import { Switch, Route, withRouter } from 'react-router';

import s from './Layout.module.scss';
import Header from '../Header';
import Footer from '../Footer/Footer';
import Sidebar from '../Sidebar';

import Admins from '../../pages/admins';
// import Kids from '../../pages/kids';
import Dashboard from '../../pages/dashboard';
import NotFound from '../../pages/notFound';
import Users from '../../pages/users';

interface LayoutState {
    sidebarOpen: boolean
}

class Layout extends React.Component<{}, LayoutState> {
  constructor(props:any) {
    super(props);

    this.state = {
      sidebarOpen: false,
    };
  }

  render() {
    return (
      <div className={s.root}>
        <Sidebar />
        <div
          className={cx(s.wrap, {[s.sidebarOpen]: this.state.sidebarOpen})}
        >
          <Header
            sidebarToggle={() =>
              this.setState({
                sidebarOpen: !this.state.sidebarOpen,
              })
            }
          />
          <main className={s.content}>
            <Switch>
              <Route path="/app/admins" component={Admins} />
              <Route path="/app/users" component={Users} />
              <Route path="/app/main" exact component={Dashboard} />
              <Route component={NotFound} />
            </Switch>
          </main>
          <Footer />
        </div>
      </div>
    );
  }
}

// @ts-ignore
export default withRouter(Layout);
